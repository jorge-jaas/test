import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: 'vista1', loadChildren: () => import('./components/first-view/first-view.module').then(m => m.FirstViewModule) },
  { path: 'vista2', loadChildren: () => import('./components/second-view/second-view.module').then(m => m.SecondViewModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
