import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FirstViewRoutingModule } from './first-view-routing.module';
import { FirstViewComponent } from './first-view.component';


@NgModule({
  declarations: [FirstViewComponent],
  imports: [
    CommonModule,
    FirstViewRoutingModule
  ]
})
export class FirstViewModule { }
