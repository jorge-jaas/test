import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SecondViewComponent } from './second-view.component';

const routes: Routes = [{ path: '', component: SecondViewComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SecondViewRoutingModule { }
